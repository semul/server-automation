#!/usr/bin/env bash

# Created By Nazmul Hasan
# Created For AntFarm AI Sdn Bhd
# Date: 08 - October - 2020

echo "Welcome to Server Setup!"
sleep 3s
echo "I am Nazmul Hasan, I am your DevOps Engineer today and I will setup a server for you now."
sleep 2s
echo "Please hit 'Enter' if needed!"
sleep 1s

echo "Adding PPA for PHP..."
sudo add-apt-repository -y ppa:ondrej/php
echo "Adding PPA for APACHE..."
sudo add-apt-repository -y ppa:ondrej/apache2
echo "Adding PPA for Certbot..."
sudo add-apt-repository -y ppa:certbot/certbot

sleep 2s

echo "Updating dependencies & required packages..."
sudo apt update -y
sudo apt dist-upgrade -y
sudo apt autoremove -y

sleep 2s

echo "Installing Required Packages..."
sudo apt-get install -y software-properties-common zip unzip curl wget nano mc

sleep 2s

echo "Installing Apache2..."
sudo apt-get install -y apache2

sleep 2s

echo "Rewrite Module Enabling..."
sudo a2enmod rewrite

sleep 2s

echo "Enter the PHP version you want to install..."
php_versions=( 7.1 7.2 7.3 7.4 )

select php_version in "${php_versions[@]}"; do
    echo "You have chosen $php_version"
    break
done

echo "Installing PHP ${php_version}..."
sudo apt-get install -y php${php_version} php${php_version}-fpm php${php_version}-cli php${php_version}-gd php${php_version}-imagick
sudo apt-get install -y php${php_version}-common php${php_version}-curl php${php_version}-mbstring
sudo apt-get install -y php${php_version}-zip php${php_version}-xml php${php_version}-imap
sudo apt-get install -y php${php_version}-mysql php${php_version}-dev php${php_version}-intl php-pear
echo "$(php -v)"

sleep 2s

echo "Installing Certbot (Free SSL)..."
sudo apt-get install -y certbot python3-certbot-apache

sleep 2s

echo "Installing Composer..."
cd ~/
curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
sudo rm composer-setup.php

sleep 2s

echo "Installing WordPress CLI..."
cd ~/
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

sleep 2s

echo "Apache Server Will Restart Now..."
sudo systemctl restart apache2

sleep 2s

clear

echo "Setup Done!"
echo "================================================="
echo "Webroot: /var/www/"
echo "Apache: /etc/apache2/"
echo "PHP: /etc/php/"
echo "================================================="